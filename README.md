# Spring Security 


## Разграничение прав

### Для регистрации админа нужно пройти по admin/register и при регистрации ввести секретный ключ - 123456. 
### Администратор сайта может забанить пользователя по имени, испольнить все дсотупные в swagger запросы.
### Зарегистрованный пользователь может испольнить запросы только /countries, /holidays, /holidays/{name} 



## Для запуска

```
cd existing_repo
git remote add origin https://gitlab.com/software-construction4/spring-security.git
git branch -M main
git push -uf origin main
docker compose build
docker compose up
```
Также необходимо запустить master ветку из проекта Calendarific API и запустить аналогично docker compose.