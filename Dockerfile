FROM amazoncorretto:21-alpine-jdk
VOLUME /tmp
EXPOSE 9090
COPY target/security-open-api-0.0.1-SNAPSHOT.jar security-open-api.jar
ENTRYPOINT ["java","-jar","/security-open-api.jar"]
