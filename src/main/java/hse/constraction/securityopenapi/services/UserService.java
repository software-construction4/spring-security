package hse.constraction.securityopenapi.services;

import hse.constraction.securityopenapi.enums.Role;
import hse.constraction.securityopenapi.models.User;
import hse.constraction.securityopenapi.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.Principal;

@Service
@RequiredArgsConstructor
public class UserService {
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    public boolean isUserUnique(User user){
        return userRepository.findByName(user.getName()) != null;
    }

    public boolean createUser(User user, boolean isAdmin) {
        if(isAdmin){
            user.getRoles().add(Role.ROLE_ADMIN);
        }
        user.setActive(true);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.getRoles().add(Role.ROLE_USER);
        userRepository.save(user);
        return true;
    }

    public User getUserByPrincipal(Principal principal){
        if (principal == null) return new User();
        return userRepository.findByName(principal.getName());
    }

    public User getUserByName(String name){
        return userRepository.findByName(name);
    }

    public void deleteUser(String name){
        userRepository.deleteUserByName(name);
    }
    public void save(User user){
        userRepository.save(user);
    }
}
