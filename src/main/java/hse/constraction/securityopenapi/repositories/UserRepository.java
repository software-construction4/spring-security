package hse.constraction.securityopenapi.repositories;

import hse.constraction.securityopenapi.models.User;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByName(String name);
    User findUserById(Long id);

    @Transactional
    @Modifying
    @Query("delete from User us where us.name = ?1")
    void deleteUserByName(String name);
}
