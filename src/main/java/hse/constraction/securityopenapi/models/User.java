package hse.constraction.securityopenapi.models;

import hse.constraction.securityopenapi.enums.Role;
import jakarta.persistence.*;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import java.time.temporal.ChronoUnit;
import jakarta.validation.constraints.NotBlank;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDate;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "users")
@Data
@Slf4j
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotBlank
    @NotBlank
    @Column(name = "name",unique = true)
    private String name;


    @Column(name = "active")
    private boolean active;

    @Column(name = "last_time_password_changed")
    private LocalDate lastPasswordChangedPassword;

    @NotBlank
    @Column(name = "password", length = 1000)
    private String password;
    @ElementCollection(targetClass = Role.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "user_role",
            joinColumns = @JoinColumn(name = "id" , referencedColumnName = "id"))
    @Enumerated(EnumType.STRING)
    private Set<Role> roles = new HashSet<>();


    public boolean hasRole(Role role){
        return roles.contains(role);
    }

    @PrePersist
    private void init() {
        lastPasswordChangedPassword = LocalDate.now();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        return roles;
    }

    @Override
    public String getUsername() {
        return name;
    }

    @Override
    public boolean isAccountNonExpired() {
        return ChronoUnit.DAYS.between(lastPasswordChangedPassword, LocalDate.now()) < 30;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return active;
    }
}