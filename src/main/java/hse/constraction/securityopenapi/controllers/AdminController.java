package hse.constraction.securityopenapi.controllers;

import hse.constraction.securityopenapi.enums.Role;
import hse.constraction.securityopenapi.models.User;
import hse.constraction.securityopenapi.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequiredArgsConstructor

public class AdminController {
    private final UserService userService;

    @PostMapping("/ban")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public void banUser(@RequestParam("userName") String userName, Principal principal){
        User userToBan = userService.getUserByName(userName);
        if ( userToBan!=null && !userToBan.hasRole(Role.ROLE_ADMIN)){
            userService.deleteUser(userName);
        }
    }
}
