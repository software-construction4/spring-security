package hse.constraction.securityopenapi.controllers;


import hse.constraction.securityopenapi.models.User;
import hse.constraction.securityopenapi.services.UserService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;




import java.util.Objects;

@Controller
@RequiredArgsConstructor
public class AuthController {
    private final UserService userService;

    @GetMapping("/login")
    public String login(Model model) {
        model.addAttribute("user", new User());
        return "login";
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            new SecurityContextLogoutHandler().logout(request, response, authentication);
        }
        return "redirect:/login";
    }

    @GetMapping("/register")
    public String registration(Model model) {
        model.addAttribute("user", new User());
        return "registration";
    }

    @PostMapping("/register")
    public String createUser(@Valid User user, BindingResult bindingResult, Model model) {
        boolean isUserValid = true;
        model.addAttribute("user", user);
        if(userService.isUserUnique(user)){
            isUserValid = false;
            model.addAttribute("isUnique", false);
        }
        if (bindingResult.hasErrors()) {
            isUserValid = false;
        }
        if(!isUserValid){
            return "registration";
        }
        userService.createUser(user,false);

        return "redirect:/login";
    }

    @GetMapping("/admin/register")
    public String adminRegister(Model model) {
        model.addAttribute("isAdminRegistration", true);
        model.addAttribute("user", new User());
        return "registration";
    }

    @PostMapping("/admin/register")
    public String createAdmin(@Valid User user, BindingResult bindingResult, @RequestParam("secretKey") String secretKey, Model model) {
        String SECRET_KEY = "123456";
        model.addAttribute("isAdminRegistration", true);
        model.addAttribute("user", user);
        boolean isUserValid = true;
        if(secretKey == null || !Objects.equals(secretKey, SECRET_KEY )){
            isUserValid=false;
            model.addAttribute("error_secretKey",true);
        }
        if(userService.isUserUnique(user)){
            isUserValid = false;
            model.addAttribute("isUnique", false);
        }
        if (bindingResult.hasErrors()) {
            isUserValid = false;
        }

        if(!isUserValid){
            return "registration";
        }
        userService.createUser(user,true);

        return "redirect:/login";
    }



}
