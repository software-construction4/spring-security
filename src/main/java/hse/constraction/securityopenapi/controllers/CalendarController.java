package hse.constraction.securityopenapi.controllers;


import hse.constraction.securityopenapi.client.rest.api.CalendaricClient;
import hse.constraction.securityopenapi.client.rest.model.Country;
import hse.constraction.securityopenapi.client.rest.model.Holiday;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class CalendarController {
    private final CalendaricClient calendaricClient;

    public CalendarController(CalendaricClient calendaricClient){
        this.calendaricClient = calendaricClient;
    }
    @GetMapping("/countries")
    public List<Country> countries(){
        return calendaricClient.countries();
    }

    @GetMapping("/holidays")
    public List<Holiday> holidays(@RequestParam(required = true) String country,
                                  @RequestParam(required = true) Integer year) {
        return calendaricClient.holidays(country, year);
    }

    @GetMapping("/holidays/{name}")
    public Holiday holidays(@RequestParam(required = true) String country,
                            @RequestParam(required = true) Integer year, @PathVariable("name") String holidayName) throws ChangeSetPersister.NotFoundException {
        return calendaricClient.holiday(country, year, holidayName);
    }

    @GetMapping("/upcoming")
    public List<Holiday> upcoming(@RequestParam(required = true) String country) {
        return calendaricClient.upcoming(country);
    }

    @GetMapping("/byType")
    public List<Holiday> holidaysByType(@RequestParam(required = true) String country,
                                        @RequestParam(required = true) Integer year, @RequestParam("type") String type){

        return calendaricClient.holidaysByType(country, year, type);
    }

    @GetMapping("/byDate")
    public List<Holiday> holidaysByDate(@RequestParam(required = true) String country,
                                        @RequestParam(required = true) Integer year, @RequestParam("month") @Min(0) @Max(12) Integer month, @RequestParam("day") @Min(0) @Max(31) Integer day){
        return calendaricClient.holidaysByDate(country, year, month, day);
    }
}
