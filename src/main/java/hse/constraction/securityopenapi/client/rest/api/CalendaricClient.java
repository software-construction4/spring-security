package hse.constraction.securityopenapi.client.rest.api;

import hse.constraction.securityopenapi.client.rest.model.Country;
import hse.constraction.securityopenapi.client.rest.model.Holiday;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "calendarific-open-api")
public interface CalendaricClient {

    @GetMapping("/countries")
    List<Country> countries();

    @GetMapping("/holidays")
    List<Holiday> holidays(@RequestParam String country,@RequestParam Integer year);

    @GetMapping("/holidays/{name}")
    Holiday holiday(@RequestParam(required = true) String country,
                            @RequestParam(required = true) Integer year, @PathVariable("name") String holidayName);

    @GetMapping("/upcoming")
    List<Holiday> upcoming(@RequestParam(required = true) String country);

    @GetMapping("/byType")
    List<Holiday> holidaysByType(@RequestParam(required = true) String country,
                                        @RequestParam(required = true) Integer year, @RequestParam("type") String type);

    @GetMapping("/byDate")
    List<Holiday> holidaysByDate(@RequestParam(required = true) String country,
                                        @RequestParam(required = true) Integer year, @RequestParam("month") @Min(0) @Max(12) Integer month, @RequestParam("day") @Min(0) @Max(31) Integer day);

}