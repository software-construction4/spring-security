package hse.constraction.securityopenapi.client.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Holiday {
    @JsonIgnore
    private Long id;

    private String name;

    private String description;

    private String countryName;

    private LocalDate date;

    private String type;

}
