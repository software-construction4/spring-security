package hse.constraction.securityopenapi.configuration;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(basePackages = "hse.constraction.securityopenapi.client.rest.api")
public class FeignClientConfiguration {

}
