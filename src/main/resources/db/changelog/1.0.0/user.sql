create table users (
   id serial primary key ,
   name varchar(255) not null unique,
   active boolean not null,
   password varchar(1000) not null
);

create table user_role (
   id bigint not null,
   role varchar(255) not null,
   foreign key (id) references users(id)
);