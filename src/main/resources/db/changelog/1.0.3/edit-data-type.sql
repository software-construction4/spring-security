alter table users drop column last_time_password_changed;
alter table users add column last_time_password_changed date;